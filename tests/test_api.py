#!/usr/bin/env python3
""" Unit tests for Custmers APIs """
import unittest


class TestCustomerAPI(unittest.TestCase):
    """ Testing Customer APIs by method """

    def setUp(self):
        """ Setup for Tests Case """
        pass

    def test_getting_root(self):
        """ unit test for GET '/' """
        pass

    def test_creating_customer(self):
        """ unit test for POST /customers """
        pass

    def test_getting_customer_by_id(self):
        """ unit test for GET /custormers/{id_customer} """
        pass

    def tearDown(self):
        """ End of Testing """
        print('end of testing')


if __name__ == '__main__':
    unittest.main()
