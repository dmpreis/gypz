#!/usr/bin/env python3
""" script for creating the databases """
from app import DB

DB.create_all()
