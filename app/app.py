#!/usr/bin/env python3
""" Customers APIs """
from flask import Flask, jsonify, request, make_response, abort
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.serializer import loads, dumps


APP = Flask(__name__)
APP.config.from_object('config.ProductionConfig')
DB = SQLAlchemy(APP)


class Customer(DB.Model):
    """ Class for handling the Customer Object Relational Mapper """
    id = DB.Column(DB.Integer, primary_key=True)
    name = DB.Column(DB.String(50), nullable=False)
    surname = DB.Column(DB.String(50), nullable=False)
    age = DB.Column(DB.Integer, nullable=False)

    def __init__(self, name, surname, age):
        self.name = name.lower().title()
        self.surname = surname.lower().title()
        self.age = abs(age)

    def __repr__(self):
        return "<Customer(name='%s', surname='%s', age='%d'>" % (self.name,
                                                                 self.surname,
                                                                 self.age)


@APP.errorhandler(404)
def not_found(error):
    """ Function to Not Found response """
    return make_response(jsonify({'error': 'Not Found'}), 404)


@APP.route('/', methods=['GET'])
def index():
    """ Function to respond the root resoure the APIs """
    return jsonify({'message': "customers"})


@APP.route('/customers', methods=['POST'])
def add_customer():
    """ API for creating a customer """
    if not request.json \
       or 'name' not in request.json \
       or 'surname' not in request.json \
       or 'age' not in request.json:
        return abort(400)
    customer = Customer(name=request.json['name'],
                        surname=request.json['surname'],
                        age=request.json['age'])
    DB.session.add(customer)
    DB.session.commit()
    return jsonify({'id': customer.id, 'name': customer.name}), 201


@APP.route('/customers/<int:id_customer>')
def list_customer_by_id(id_customer):
    """ API for retrieving a customer by id """
    customer = Customer.query.get(id_customer)
    if not customer:
        return abort(404)
    return jsonify({'id': customer.id,
                    'age': customer.age,
                    'name': customer.name,
                    'surname': customer.surname}), 200


if __name__ == '__main__':
    APP.run()
