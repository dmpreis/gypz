#!/usr/bin/env python3
""" Module with the configuration's parameter of the API """


class Config(object):
    """ Default Configuration """
    DEBUG = False
    TESTING = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/dev-customer.db'


class ProductionConfig(Config):
    """ Production Environment Configuration """
    SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/customer.db'


class DevelopmentConfig(Config):
    """ Development EnvironmentConfiguration """
    DEBUG = True


class TestingConfig(Config):
    """ Testing Environment Configuration """
    TESTING = True
