# GYPZ LAB - Teste DEVOPS
Stack de CI/CD para o teste DEVOPS para GYPZ LAB.
# Conteúdo
 * [Estrutura do Projeto](#estrutura-projeto)
 * [Stack Utilizada](#stack-utilizada)
 * [O pipeline](#pipelinet)
 * [Deploy](#deploy)
 * [Como Testar](#como-testar)
# Estrutura do Projeto
O projeto tem a seguinte estrutura
```gypz/
 app/
 * app.py -----------> fonte da API
 * requirements.txt -> arquivo de dependencias
 * setupdb.py -------> script de criação do banco de dados
 deployment/
 * deploy.sh --------> script bash para provionamento da instância na AWS 
 * user_data.txt ----> script com os comandos de inicialização da instância
 dockerfiles/
 * Dockerfile -------> arquivo com as instrução de build da imagem da API
 .gitlab-ci.yml ------> arquivo contendo a definção do pipeline de CI/CD
 tests/
 * test_api.py ------> testes unitários
 ```
# Stack utilizada
Para realização do teste foi utilizada a seguinte stack:
 * IDE:
	 * Vim
 * API:
	 * Python 3.6
	 * Flask
	 * Sqlite
 * Pipeline CI/CD:
	 * Gitlab CI/CD
 * Local de Deploy:
	 * AWS

# O Pipeline
## Ferramenta escolhida
Para construção do pipeline foi utilizado o Gtilab CI, por conta  da integração com o versionamento de código, isto é, no próprio repositório do projeto podemos definir o pipeline e acompnhar sua execução. Outro ponto é a possibilidade de se utilizar as diversas variaveis de ambiente do prórprio Gitlab e/ou criar suas proprias variaveis no projeto. 
Para mais informações acesse [aqui](https://docs.gitlab.com/ee/ci/variables/).
## Stages do Pipeline
Os stages são os seguintes: 
```yaml
image: python:3.6.9-alpine3.10`
stages:
 - test
 - build
 - deploy
```
Por conta de estarmos construindo uma API em Python/Flask pipeline foi escolhido a imagem docker python:3.6.9-alpine3.10  como base de execução do pipeline.
## Jobs
### style_qa
Esse job é responsável por verificar a quialidade do código juntamente com o estilo padrão de codificação PEP-8.
A ferramenta utilizada para essa etapa foi a [flake8](http://flake8.pycqa.org/en/latest/).
### coverage
Nesse job é feita  a análise da cobertura de código por meio do [coverage](https://coverage.readthedocs.io/en/v4.5.x/). O resultado é exportado como artefato com duração de 15 dias dentro do prórpio Gitlab. O relatório pode ser verificado [aqui](https://gitlab.com/dmpreis/gypz/-/jobs/289005475/artifacts/browse).
### unit_tests
Job responsáel pela execução dos testes unitários, foi utilizado o módulo [unittest](https://docs.python.org/3/library/unittest.html) do próprio Python.
### build_image
Esse job é reponsável por construir a imagem docker e publicar no [DockerHub](https://cloud.docker.com/u/dmpreis/repository/docker/dmpreis/gypz_api) de acordo com a branch/tag onde o commit foi realizado. Escolhi seguir por esse caminho, pois assim temos uma imagem de acordo com o momento do projeto, por exemplo, caso esteja trabalhando na branch development a  imagem gerada será dmpreis/gypz_api:development.
### build_prod_image
Esse job roda exclusivamente quando uma tag é gerada e o produto do processamento é a imagem dmpreis/gypz_api:latest que será implantada no ambiente de produção. 
### deploy_staging
Esse job é reposnsável por provisionar uma instância na AWS para o ambiente de staging do projeto. 
O provisionamento é feito com awscli através dos arquivos deploy.sh e user_data.txt.
```sh
deploy.sh
#!/bin/bash
HSTNM='GYPZ-API'
aws ec2 run-instances \
 --image-id ami-0b60e7d34d8ba6db4 \
 --count 1 --instance-type t2.micro  \
 --region sa-east-1 \
 --security-group-ids 'sg-63fe031a' \
 --subnet-id 'subnet-36ecf651' \
 --monitoring Enabled=true \
 --key-name $PRIVATE_KEY \
 --user-data file://user_data.txt \
 --tag-specifications "ResourceType=instance,Tags=[{Key=Name,Value=$HSTNM}]"
```
```sh
user_data.txt
#!/bin/bash
yum update -y && yum update -y
INTANCE_ID=($curl -s http://169.254.169.254/latest/meta-data/instance-id)
aws ec2 associate-address --instance-id $INTANCE_ID --allocation eipalloc-00265ac12a5f9052b
sudo amazon-linux-extras install -y docker
sudo service docker start
sudo docker container run -d -p80:5000 -v db:/tmp dmpreis/gypz_api:master
Esse job roda somente quando é feito um commit na branch master e utiliza a imagem de staging
```
### deploy_production
Tem a mesma função do job acima, porém o provisionamento é feito em uma outra instância, só é executado quando é gerado uma nova tag no projeto e a imagem utilizada é a dmpreis/gypz_api:latest.

**_NOTA:_**  Esse pipeline é uma prova de  conceito de como se pode implementar Continuous Integration e Continuous Deployment em um projeto, porém vale ressaltar que para aplicações produtivas é interessante darmos mais robustez para o pipeline com ferramentas de DAST e SAST, por exemplo Fortify e SonarQube, implementar gates de validação de risco de aplicação (SD Elements).
Para o deploy foi utilizado somente AWS CLI, por ser uma prova de conceito, porém a própria AWS oferece serviços como o Code Deploy e o Elastic Beanstalk para prover instalação de aplicação de maneira mais resiliente e caso prefira uma abordagem mais agnóstica podemos utilizar ferramentas como Ansible ou Terraform
# Deploy
A aplicação foi instalada na AWS, pois podemos fazer uso do CloudWatch para monitoramento da aplicação gerando Dashboards e estabelecendo alertas para assim termos uma aplicação mais resiliente.
Vale ressaltar que essa não é a única solução disponível, existem outro cloud providers e para monitoração podemos utilizar ferramentas como Prometheus com Grafana e Netdata para atingirmos o mesmo objeto e evitando vendor lockin. 
# Como Testar
Para testar a api gerada é possível rodar a imagem gerada pelo pipeline localmente ou acessar a URL de produção.
## Request Local
**_NOTA:_** É necessário ter o Docker instalado em sua máquina, caso não tenha os passos podem ser seguidos [aqui](https://docs.docker.com/install/).
Executar o seguinte comando:
```console
foo@bar:~$ docker container run -ti -p80:5000 -v db:/tmp dmpreis/gypz_api
* Serving Flask app "app.py"
* Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
* Debug mode: off
/usr/local/lib/python3.6/site-packages/flask_sqlalchemy/__init__.py:835: FSADeprecationWarning: SQLALCHEMY_TRACK_MODIFICATIONS adds significant overhead and will be disabled by default in the future.  Set it to True or False to suppress this warning.
  'SQLALCHEMY_TRACK_MODIFICATIONS adds significant overhead and '
* Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
```
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/6dd9701a111df09320e8)

Se preferir testar via terminal:
```console
#Teste de chamada
foo@bar:~$ curl http://localhost/
#criando um novo cliente
foo@bar:~$ curl -H "Content-Type:application/json" -X POST -d'{"name":"nome", "surname":"sobrenome", "age":99}' http:localhost/customers
#pesquisando um clinte pelo id
foo@bar:~$ curl http://localhost/customers/1
```

##  Request Staging
Está disponível o endereço http://ec2-18-231-116-173.sa-east-1.compute.amazonaws.com/ para acesso ao ambiente produtivo com a ultima versão da imagem gerada.

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/643dad3d626ba3258319)

##  Request Produção
Está disponível o endereço http://api.dmprlab.org/ para acesso ao ambiente produtivo com a ultima versão da imagem gerada.

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/9995d22cb43dc6c6025d)
